This module will show you the list of SDC with its Name, Definition, Type and Path.

Install and enable like any other module.

Navigate to /admin/config/sdc-list to view the list of Single Directory Components available in your project.
