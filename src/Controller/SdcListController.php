<?php

namespace Drupal\sdc_list\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sdc\ComponentPluginManager;

/**
 * SdcListController return table.
 */
class SdcListController extends ControllerBase {


  public function list() {
    $plugin_manager = \Drupal::service('plugin.manager.sdc');
    assert($plugin_manager instanceof ComponentPluginManager);
    $components = $plugin_manager->getAllComponents();

    $rows = [];
    foreach ($components as $component) {
      $plugin_definition = $component->getPluginDefinition();
      $name = $plugin_definition['name'];
      $description = $plugin_definition['description'];
      $type = $plugin_definition['extension_type'];
      $typename = $type->name;
      $provider = $plugin_definition['provider'];
      $template = $component->getTemplatePath();

      $row = [
        $name,
        $provider,
        $typename,
        $template,
      ];

      array_push($rows, $row);
    }


    $header = [
      'col1' => t('Component'),
      'col2' => t('Defined In'),
      'col3' => t('Type'),
      'col4' => t('Template Path'),
    ];

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('There are no items yet.'),
      '#tabledrag' => array(
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'mytable-order-weight',
        ),
      ),
    ];

  }

}
